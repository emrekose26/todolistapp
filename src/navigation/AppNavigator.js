import React from "react";
import { Platform } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import { createAppContainer } from "react-navigation";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";

import Colors from "../constants/colors";

import AllTasksScreen from "../screens/AllTasks/AllTasksScreen";
import TodayTasksScreen from "../screens/TodayTasks/TodayTasksScreen";
import WeeklyTasksScreen from "../screens/WeeklyTasks/WeeklyTasksScreen";
import MonthlyTasksScreen from "../screens/MonthlyTasks/MonthlyTasksScreen";

const bottomTabConfig = {
  AllTasks: {
    screen: AllTasksScreen,
    navigationOptions: {
      tabBarColor: Colors.primary,
      tabBarIcon: tabInfo => {
        return (
          <MaterialCommunityIcons
            name="calendar-text"
            size={30}
            style={{ color: tabInfo.tintColor }}
          />
        );
      },
      tabBarLabel: Platform.OS === "android" ? <Text>All</Text> : "All"
    }
  },
  TodayTasks: {
    screen: TodayTasksScreen,
    navigationOptions: {
      tabBarColor: Colors.primary,
      tabBarIcon: tabInfo => {
        return (
          <MaterialCommunityIcons
            name="calendar-today"
            size={30}
            style={{ color: tabInfo.tintColor }}
          />
        );
      },
      tabBarLabel: Platform.OS === "android" ? <Text>Today</Text> : "Today"
    }
  },
  WeeklyTasks: {
    screen: WeeklyTasksScreen,
    navigationOptions: {
      tabBarColor: Colors.primary,
      tabBarIcon: tabInfo => {
        return (
          <MaterialCommunityIcons
            name="calendar-week"
            size={30}
            style={{ color: tabInfo.tintColor }}
          />
        );
      },
      tabBarLabel: Platform.OS === "android" ? <Text>Week</Text> : "Week"
    }
  },
  MonthlyTasks: {
    screen: MonthlyTasksScreen,
    navigationOptions: {
      tabBarColor: Colors.primary,
      tabBarIcon: tabInfo => {
        return (
          <MaterialCommunityIcons
            name="calendar-text"
            size={30}
            style={{ color: tabInfo.tintColor }}
          />
        );
      },
      tabBarLabel: Platform.OS === "android" ? <Text>Month</Text> : "Month"
    }
  }
};

const BottomNavigator =
  Platform.OS === "ios"
    ? createBottomTabNavigator(bottomTabConfig, {
        tabBarOptions: {
          activeTintColor: Colors.accent,
          inactiveTintColor: "white",
          style: {
            backgroundColor: Colors.primary
          }
        }
      })
    : createMaterialBottomTabNavigator(bottomTabConfig, {
        activeTintColor: Colors.accent
      });

export default createAppContainer(BottomNavigator);
