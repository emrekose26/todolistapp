import React from "react";
import { View, SafeAreaView, Modal } from "react-native";
import {
  Container,
  Content,
  Item,
  Input,
  Button,
  DatePicker,
  Text,
  Card,
  Body,
  CardItem
} from "native-base";

const AddNewTodo = props => {
  return (
    <SafeAreaView>
      <Modal
        presentationStyle="formSheet"
        animationType="slide"
        visible={props.mVisible}
      >
        <Card style={{ marginTop: 35, padding: 50, justifyContent: "center" }}>
          <CardItem header>
            <Text>Add New Todo</Text>
          </CardItem>
          <Item regular>
            <Input
              placeholder="To do"
              value={props.text}
              onChangeText={props.onTodoTextChange}
            />
          </Item>

          <DatePicker
            modalTransparent={false}
            animationType={"fade"}
            androidMode={"default"}
            placeHolderText="Select date"
            textStyle={{ color: "green" }}
            placeHolderTextStyle={{ color: "#d3d3d3" }}
            onDateChange={props.onTodoDateChange}
            disabled={false}
          />
          
          <Button
          style={{marginVertical: 5}}
            onPress={() => {
              props.addTodo();
              props.onVisibleStateChange(!props.mVisible);
            }}
          >
            <Text>Add Todo</Text>
          </Button>

          <Button
            danger
            onPress={() => {
              props.onVisibleStateChange(!props.mVisible);
            }}
          >
            <Text>Cancel</Text>
          </Button>
        </Card>
      </Modal>
    </SafeAreaView>
  );
};

export default AddNewTodo;
