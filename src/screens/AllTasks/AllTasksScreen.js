import React, { useState, useEffect } from "react";
import {
  View,
  TouchableOpacity
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Right,
  Icon,
  Title,
  Input
} from "native-base";

import { SwipeListView } from "react-native-swipe-list-view";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import ListItem from "../../components/ListItem";
import AddNewTodo from "../../components/AddNewTodo";
import styles from "./styles";

import { openDatabase } from "react-native-sqlite-storage";
var db = openDatabase({ name: "Todos.db" });
import { addTodo, deleteTodo, initDb } from "../../database/db";

const AllTasksScreen = props => {
  const [modalVisible, setModalVisible] = useState(false);
  const [todoText, settodoText] = useState("");
  const [chosenDate, setChosenDate] = useState(new Date());
  const [allTodos, setAllTodos] = useState([]);

  useEffect(() => {
    initDb();
  });

  useEffect(() => {
    db.transaction(tx => {
      tx.executeSql("SELECT * FROM todos", [], (tx, results) => {
        var temp = [];
        for (let i = 0; i < results.rows.length; ++i) {
          temp.push(results.rows.item(i));
        }
        setAllTodos(temp);
      });
    });
  });

  return (
    <Container>
      <Header>
        <Left />
        <Body>
          <Title>All Tasks</Title>
        </Body>
        <Right>
          <Button transparent onPress={() => setModalVisible(true)}>
            <Icon name="add" />
          </Button>
        </Right>
      </Header>

      <AddNewTodo
        addTodo={() => addTodo(todoText, chosenDate)}
        onVisibleStateChange={state => setModalVisible(state)}
        mVisible={modalVisible}
        text={todoText}
        date={chosenDate}
        onTodoDateChange={d => setChosenDate(d)}
        onTodoTextChange={t => settodoText(t)}
      />

      <SwipeListView
        data={allTodos}
        keyExtractor={(item, index) => index.toString()}
        renderItem={(data, rowMap) => <ListItem todoItem={data} />}
        renderHiddenItem={(data, rowMap) => (
          <View style={styles.rowBack}>
            <TouchableOpacity onPress={() => console.log("update")}>
              <MaterialCommunityIcons name="update" size={30} color="green" />
            </TouchableOpacity>

            <TouchableOpacity onPress={() => deleteTodo(data.item.todoId)}>
              <MaterialCommunityIcons name="trash-can" size={30} color="red" />
            </TouchableOpacity>
          </View>
        )}
        leftOpenValue={75}
        rightOpenValue={-75}
      />
    </Container>
  );
};

export default AllTasksScreen;
