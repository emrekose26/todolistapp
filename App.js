import React from "react";
import AppNavigator from "./src/navigation/AppNavigator";
import { Root } from "native-base";

export default function App() {
  return (
    <Root>
      <AppNavigator />
    </Root>
  );
}
