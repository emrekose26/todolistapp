import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
  rowFront: {
    backgroundColor: "#fafafa",
    padding: 10,
    marginVertical: 1,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    justifyContent: "center",
    height: 60
  },
  rowBack: {
    alignItems: "center",
    backgroundColor: "#DDD",
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 15
  }
});
