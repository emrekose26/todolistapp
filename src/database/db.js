import React from "react";
import { Alert } from "react-native";

import { openDatabase } from "react-native-sqlite-storage";
var db = openDatabase({ name: "Todos.db" });

export function initDb() {
  db.transaction(txn => {
    txn.executeSql(
      "SELECT name FROM sqlite_master WHERE type='table' AND name='todos'",
      [],
      function(tx, res) {
        if (res.rows.length == 0) {
          txn.executeSql("DROP TABLE IF EXISTS todos", []);
          txn.executeSql(
            "CREATE TABLE IF NOT EXISTS todos(todoId INTEGER PRIMARY KEY AUTOINCREMENT, todoName VARCHAR(255), todoDate VARCHAR(75))",
            []
          );
        }
      }
    );
  });
}

export function addTodo(name, date) {
  db.transaction(tx => {
    tx.executeSql(
      "INSERT INTO todos (todoName, todoDate) VALUES (?,?)",
      [name, date.toString()],
      (tx, results) => {
        if (results.rowsAffected > 0) {
          Alert.alert("Success", "Todo added successfully", [{ text: "Ok" }], {
            cancelable: false
          });
        } else {
          alert("Failed");
        }
      }
    );
  });
}

export function deleteTodo(id) {
  db.transaction(tx => {
    tx.executeSql("DELETE FROM todos where todoId=?", [id], (tx, results) => {
      if (results.rowsAffected > 0) {
        Alert.alert("Success", "Todo deleted successfully", [{ text: "Ok" }], {
          cancelable: false
        });
      } else {
        alert("Please insert a valid todo Id");
      }
    });
  });
}
