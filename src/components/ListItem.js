import React from "react";
import { View, Text, StyleSheet } from "react-native";

const ListItem = props => {
  return (
    <View style={styles.rowFront}>
      <Text style={{ fontSize: 18, color: "black" }}>
        {props.todoItem.item.todoName}
      </Text>
      <Text style={{ fontSize: 12, color: "gray" }}>
        {props.todoItem.item.todoDate.toString().slice(0, 15)}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  rowFront: {
    backgroundColor: "#fafafa",
    padding: 10,
    marginVertical: 1,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    justifyContent: "center",
    height: 60
  }
});

export default ListItem;
